<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20230523031918 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE factura (id INT AUTO_INCREMENT NOT NULL, establecimiento VARCHAR(3) NOT NULL, punto_emision VARCHAR(3) NOT NULL, secuencial INT NOT NULL, fecha_emision DATETIME NOT NULL, subtotal NUMERIC(18, 2) NOT NULL, impuesto NUMERIC(18, 2) NOT NULL, total NUMERIC(18, 2) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE guia (id INT AUTO_INCREMENT NOT NULL, numero_guia VARCHAR(10) NOT NULL, fecha_envio DATETIME NOT NULL, pais_origen VARCHAR(100) NOT NULL, nombre_remitente VARCHAR(100) NOT NULL, direccion_remitente VARCHAR(100) NOT NULL, telefono_remitente VARCHAR(50) DEFAULT NULL, email_remitente VARCHAR(50) DEFAULT NULL, pais_destino VARCHAR(100) NOT NULL, nombre_destinatario VARCHAR(100) NOT NULL, direccion_destinatario VARCHAR(100) NOT NULL, telefono_destinatario VARCHAR(50) DEFAULT NULL, email_destinatario VARCHAR(50) DEFAULT NULL, total NUMERIC(18, 2) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE guia_factura (guia_id INT NOT NULL, factura_id INT NOT NULL, INDEX IDX_2601991962AA81F (guia_id), INDEX IDX_26019919F04F795F (factura_id), PRIMARY KEY(guia_id, factura_id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE pago (id INT AUTO_INCREMENT NOT NULL, id_factura_id INT NOT NULL, tipo_pago VARCHAR(10) NOT NULL, valor NUMERIC(18, 2) NOT NULL, INDEX IDX_F4DF5F3E55C5F988 (id_factura_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE messenger_messages (id BIGINT AUTO_INCREMENT NOT NULL, body LONGTEXT NOT NULL, headers LONGTEXT NOT NULL, queue_name VARCHAR(190) NOT NULL, created_at DATETIME NOT NULL, available_at DATETIME NOT NULL, delivered_at DATETIME DEFAULT NULL, INDEX IDX_75EA56E0FB7336F0 (queue_name), INDEX IDX_75EA56E0E3BD61CE (available_at), INDEX IDX_75EA56E016BA31DB (delivered_at), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE guia_factura ADD CONSTRAINT FK_2601991962AA81F FOREIGN KEY (guia_id) REFERENCES guia (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE guia_factura ADD CONSTRAINT FK_26019919F04F795F FOREIGN KEY (factura_id) REFERENCES factura (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE pago ADD CONSTRAINT FK_F4DF5F3E55C5F988 FOREIGN KEY (id_factura_id) REFERENCES factura (id)');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE guia_factura DROP FOREIGN KEY FK_2601991962AA81F');
        $this->addSql('ALTER TABLE guia_factura DROP FOREIGN KEY FK_26019919F04F795F');
        $this->addSql('ALTER TABLE pago DROP FOREIGN KEY FK_F4DF5F3E55C5F988');
        $this->addSql('DROP TABLE factura');
        $this->addSql('DROP TABLE guia');
        $this->addSql('DROP TABLE guia_factura');
        $this->addSql('DROP TABLE pago');
        $this->addSql('DROP TABLE messenger_messages');
    }
}
