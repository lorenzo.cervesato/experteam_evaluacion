/*
 * Welcome to your app's main JavaScript file!
 *
 * We recommend including the built version of this JavaScript file
 * (and its CSS file) in your base layout (base.html.twig).
 */

// any CSS you import will output into a single css file (app.css in this case)
import './styles/app.css';

// start the Stimulus application
import './bootstrap';


/* ### CUSTOM CODIGO JS  ### */

// Attente de 2 secondes avant de supprimer les messages flash
setTimeout(function() {
  var alertElements = document.querySelectorAll('.alert-dismissible');
  alertElements.forEach(function(alertElement) {
    alertElement.classList.add('fade-out');
    // Supprimer l'élément après la fin de la transition
    alertElement.addEventListener('transitionend', function() {
      alertElement.remove();
    });
  });
}, 1000);

/* ### /CUSTOM CODIGO JS  ### */
