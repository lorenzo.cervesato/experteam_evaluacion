<?php

namespace App\Utils;

use App\Repository\GuiaRepository;

class MyFunctions {

    public function calculaSubtotal($lista_guia):float
    {
        $subtotal = 0;
        foreach($lista_guia as $cur_guia){
            $subtotal += $cur_guia->getTotal();
        }
        unset($lista_guia);
        return $subtotal;
    }

    public function calculaTotal($subtotal, $impuesto):float
    {
        $total = $subtotal + ($impuesto*$subtotal/100);
        return $total;
    }

    public function calculaImpuesto($subtotal, $impuesto):float
    {
        $totalImpuesto = ($impuesto*$subtotal/100);
        return $totalImpuesto;
    }

    public function createFiltros (GuiaRepository $guia_repo): mixed
    {
        $filtros = [
            'criteria' => [
                'PaisDestino' => [],
                'PaisOrigen' => []
            ],
            /*
            'orderby' => [
                'FechaEnvio' => 'ASC',
                'FechaEnvio' => 'DESC'
            ],
            */
        ];
        
        $filtros_pais_dest = $guia_repo->findDistinctPaisDestino();

        foreach( $filtros_pais_dest as $pais_dest){
            array_push($filtros['criteria']['PaisDestino'], $pais_dest['PaisDestino']);
        }

        $filtros_pais_orig = $guia_repo->findDistinctPaisOrigen();

        foreach( $filtros_pais_orig as $pais_orig){
            array_push($filtros['criteria']['PaisOrigen'], $pais_orig['PaisOrigen']);
        }

        return $filtros;
    }
}