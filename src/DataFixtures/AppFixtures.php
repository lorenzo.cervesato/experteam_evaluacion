<?php

namespace App\DataFixtures;

use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;
use App\Entity\Guia;
use phpDocumentor\Reflection\Types\Array_;

class AppFixtures extends Fixture
{
    private function select_pais(): mixed
    {
        $tbl_paises = ["Alemania","Argentina","Australia","Austria","Bélgica","Colombia","Cuba","Dinamarca","Ecuador","España","Estados Unidos","Francia","Italia","Jamaica","Perú","Portugal","Reino Unido","Venezuela"];
        
        $rand_index_selected1 = rand(0, count($tbl_paises)-1);
        $pais_selected1 = $tbl_paises[$rand_index_selected1];
        unset($tbl_paises[$rand_index_selected1]);

        $rand_index_selected2 = rand(0, count($tbl_paises)-1);
        $pais_selected2 = $tbl_paises[$rand_index_selected2];
        unset($tbl_paises[$rand_index_selected2]);

        return [$pais_selected1, $pais_selected2];
    }

    public function load(ObjectManager $manager): void
    {
        $guias_data = array();
        $numeroGuia = 101;
        

        for($i=0; $i<10; $i++){
            $datetime = new \DateTime('01/01/2023');
            $datetime->setTime(12,00,00);
            $datetime->modify('+'.$i.' day');
            $paises = $this->select_pais();

            array_push($guias_data, [
                "NumeroGuia" => $numeroGuia + $i,
                "FechaEnvio" => $datetime,
                "PaisOrigen" => $paises[0],
                "NombreRemitente" => "Alberto Felipe Muños{$i}",
                "DireccionRemitente" => "10{$i} calle Bolivar",
                "TelefonoRemitente" => "091020300{$i}",
                "EmailRemitente" => "alberto{$i}@gmail.com",
                "PaisDestino" => $paises[1],
                "NombreDestinatario" => "Juan Luis Guerra{$i}",
                "DireccionDestinatario" => "10{$i} calle Olmedo",
                "TelefonoDestinatario" => "091020304{$i}",
                "EmailDestinatario" => "juan{$i}@gmail.com",
                "Total" => "5{$i}"
            ]) ;
        }

        foreach($guias_data as $k => $cur_guia){
            $Guia = new Guia();
            $Guia->setNumeroGuia((string)$cur_guia['NumeroGuia']);
            $Guia->setFechaEnvio($cur_guia['FechaEnvio']);
            $Guia->setPaisOrigen($cur_guia['PaisOrigen']);
            $Guia->setNombreRemitente($cur_guia['NombreRemitente']);
            $Guia->setDireccionRemitente($cur_guia['DireccionRemitente']);
            $Guia->setTelefonoRemitente($cur_guia['TelefonoRemitente']);
            $Guia->setEmailRemitente($cur_guia['EmailRemitente']);
            $Guia->setPaisDestino($cur_guia['PaisDestino']);
            $Guia->setNombreDestinatario($cur_guia['NombreDestinatario']);
            $Guia->setDireccionDestinatario($cur_guia['DireccionDestinatario']);
            $Guia->setTelefonoDestinatario($cur_guia['TelefonoDestinatario']);
            $Guia->setEmailDestinatario($cur_guia['EmailDestinatario']);
            $Guia->setTotal((float)$cur_guia['Total']);
            $manager->persist($Guia);
        }

        $manager->flush();
    }
}
