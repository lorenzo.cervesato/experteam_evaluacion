<?php

namespace App\Controller;

use App\Entity\Pago;
use App\Form\PagoType;
use App\Repository\PagoRepository;
use App\Repository\FacturaRepository;
use Doctrine\Persistence\ObjectManager;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class PagosController extends AbstractController
{
    #[Route('/pagos', name: 'pagos.index')]
    #[Route('/pagos/edit/{id}', name: 'pagos.edit')]
    public function index(Pago $Pago = null, Request $request, PagoRepository $Pago_repo, FacturaRepository $factura_repo, ObjectManager $manager, $map_facturas = []): Response
    {
        $lista_pagos = $Pago_repo->findAll();
        $lista_factura = $factura_repo->findAll();

        foreach ($lista_factura as $curFactura) {
            $map_facturas['Factura - '. (string) $curFactura->getSecuencial().' - '. (string) $curFactura->getTotal().'$'] = (string) $curFactura->getId();
        }

        $options['map_facturas'] = $map_facturas;

        $formPago = $this->createForm(PagoType::class, null, $options);

        $modoEdit = false;

        // Envoi en POST pour soumettre le formulaire d'ajout
        if($request->request->count() > 0 && $Pago === null){
            //dd('test1', $request, $Pago);
            $formPago->handleRequest($request);
        }

        // Envoi en GET pour charger le formulaire
        if($request->request->count() === 0 && $Pago !== null){
            //dd('test2', $request, $Pago);
            $modoEdit = true;
            $formPago = $this->createForm(PagoType::class, $Pago, $options);
        }

        // Envoi en POST pour soumettre le formulaire de modif
        if($request->request->count() > 0 && $Pago !== null){
            //dd('test3', $request, $Pago);
            $formPago = $this->createForm(PagoType::class, $Pago, $options);
            $formPago->handleRequest($request);
        }

        // Fonction qui traite tous les cas d'envois.
        if($formPago->isSubmitted() && $formPago->isValid()){
            //dd('test4', $request, $Pago);
            $Pago = $formPago->getData();
            $req_data = $request->request->all();
            $Pago->setTipoPago($req_data['pago']['TipoPago']);

            foreach($lista_factura as $factura){
                if($req_data['pago']['IdFactura'] == $factura->getId()){
                    $Pago->setIdFactura($factura);
                }
            }

            $manager->persist($Pago);
            $manager->flush();

            // Mensaje flash
            $this->addFlash('info', 'Pago registrado!');

            return $this->redirectToRoute('pagos.index');
        }

        return $this->render('pages/pagos.html.twig', [
            'controller_name' => 'PagosController',
            'modoEdit' => $modoEdit,
            'lista_pagos' => $lista_pagos,
            'formPago' => $formPago->createView()
        ]);
    }
}
