<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use App\Repository\GuiaRepository;
use App\Repository\FacturaRepository;
use Doctrine\Persistence\ObjectManager;
use App\Utils\MyFunctions;
use App\Utils\Constants;
use App\Entity\Factura;
use App\Form\FacturaType;

class FacturasController extends AbstractController
{
    #[Route('/facturas', name: 'facturas.index')]
    #[Route('/facturas/edit/{id}', name: 'facturas.edit')]
    
    public function index(Factura $Factura = null, Request $request, GuiaRepository $guia_repo, FacturaRepository $factura_repo, ObjectManager $manager): Response
    {
        $lista_factura = $factura_repo->findAll();
        $lista_guia = $guia_repo->findAll();
        $map_guias = [];
        $options = [];

        foreach ($lista_guia as $curGuia) {
            $map_guias['Guia - '.$curGuia->getNumeroGuia()] = (string) $curGuia->getId();
        }

        $options['map_guias'] = $map_guias;
        // $options['data'] = null;

        $formFactura = $this->createForm(FacturaType::class, null, $options);

        $modoEdit = false;

        // Envoi en POST pour soumettre le formulaire d'ajout
        if($request->request->count() > 0 && $Factura === null){
            //dd('test1',$request);
            $formFactura->handleRequest($request);
        }

        // Envoi en GET pour charger le formulaire
        if($request->request->count() === 0 && $Factura !== null){
            //dd('test2', $request, $Factura);
            //*
            $data_selected = $Factura->getListaGuia()->toArray();
            
            foreach ($data_selected as $key => $cur_data) {
                $tbl_ids[$key] = (string) $cur_data->getId();
            }
            //*/

            // $options['data'] = $tbl_ids;

            $modoEdit = true;
            $formFactura = $this->createForm(FacturaType::class, $Factura, $options);
        }

        // Envoi en POST pour soumettre le formulaire de modif
        if($request->request->count() > 0 && $Factura !== null){
            // dd('test3', $request, $Factura);
            $formFactura = $this->createForm(FacturaType::class, $Factura, $options);
            $formFactura->handleRequest($request);
        }

        // Fonction qui traite tous les cas d'envois.
        if($formFactura->isSubmitted() && $formFactura->isValid()){
            
            // dd('test4.1', $request, $Factura);
            $Factura = $formFactura->getData();
            $MyFunctions = new MyFunctions();

            $req_data = $request->request->all();
            // dd('test4.1', $request, $Factura);
            foreach($lista_guia as $curGuia){
                foreach($req_data['factura']['ListaGuia'] as $id_guias_selected){
                    if($id_guias_selected == $curGuia->getId()){
                        $Factura->addGuia($curGuia);
                    }
                }
            }

            //dd('test4.2', $request, $Factura);
           
            $Factura->setSubtotal(
                $MyFunctions->calculaSubtotal(
                    $Factura->getListaGuia()
                )
            );
            $Factura->setImpuesto(
                $MyFunctions->calculaImpuesto($Factura->getSubtotal(), IMPUESTO)
            );
            $Factura->setTotal(
                $MyFunctions->calculaTotal($Factura->getSubtotal(), IMPUESTO)
            );
            
            $manager->persist($Factura);
            $manager->flush();
            $this->addFlash('info', 'Factura registrada!');
            return $this->redirectToRoute('facturas.index');
        }

        return $this->render('pages/facturas.html.twig', [
            'controller_name' => 'FacturasController',
            'modoEdit' => $modoEdit,
            'lista_facturas' => $lista_factura,
            'formFactura' => $formFactura->createView(),
        ]);
    }

    #[Route('/facturas/delete/{id}', name: 'facturas.delete')]
    public function delete(Factura $Factura = null,  ObjectManager $manager):Response
    {
        $manager->remove($Factura);
        $manager->flush();
        $this->addFlash('info', 'Factura Eliminada!');

        return $this->redirectToRoute('facturas.index');
    }
}
