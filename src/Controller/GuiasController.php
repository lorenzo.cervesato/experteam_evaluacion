<?php

namespace App\Controller;

use App\Entity\Guia;
use App\Form\GuiaType;
use App\Utils\MyFunctions;
use App\Repository\GuiaRepository;
use Doctrine\Persistence\ObjectManager;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class GuiasController extends AbstractController
{
    
    #[Route('/guias', name: 'guias.index')]
    #[Route('/guias/edit/{id}', name: 'guias.edit')]
    #[Route('/guias/filtro/{opcion_filtro}/{filtro_selected}', name: 'guias.filtro')]
    public function index(Guia $Guia = null, Request $request, GuiaRepository $guia_repo, ObjectManager $manager, $opcion_filtro = null, $filtro_selected = null): Response
    {
        $MyFunctions = new MyFunctions();

        $criteria = ($opcion_filtro != null && $filtro_selected != null) ? [$opcion_filtro => $filtro_selected] : [];

        $filtro = ['criteria'=> $criteria,
                'orderby' => ['FechaEnvio' => 'DESC']];

        $lista_guias = $guia_repo->findBy($filtro['criteria'], $filtro['orderby']);

        $opciones_filtros = $MyFunctions->createFiltros($guia_repo);

        $formGuia = $this->createForm(GuiaType::class);

        $modoEdit = false;
        
        // Envoi en POST pour soumettre le formulaire d'ajout
        if($request->request->count() > 0 && $Guia === null){
            //dd('test1',$request);
            $formGuia->handleRequest($request);
        }

        // Envoi en GET pour charger le formulaire
        if($request->request->count() === 0 && $Guia !== null){
            $modoEdit = true;
            $formGuia = $this->createForm(GuiaType::class, $Guia);
        }

        // Envoi en POST pour soumettre le formulaire de modif
        if($request->request->count() > 0 && $Guia !== null){
            $formGuia = $this->createForm(GuiaType::class, $Guia);
            $formGuia->handleRequest($request);
        }

        // Fonction qui traite tous les cas d'envois.
        if($formGuia->isSubmitted() && $formGuia->isValid()){
            $Guia = $formGuia->getData();

            $manager->persist($Guia);
            $manager->flush();
            // Mensaje flash
            $this->addFlash('info', 'Guía registrada!');

            return $this->redirectToRoute('guias.index');
        }
        

        return $this->render('pages/guias.html.twig', [
                'controller_name' => 'GuiasController',
                'modoEdit' => $modoEdit,
                'lista_guias' => $lista_guias,
                'formGuia' => $formGuia->createView(),
                'opciones_filtros' => $opciones_filtros
            ]);
    }
}
