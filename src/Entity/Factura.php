<?php

namespace App\Entity;

use App\Repository\FacturaRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\DBAL\Types\Types;
use Doctrine\ORM\Mapping as ORM;
use App\Entity\Guia;
use Doctrine\DBAL\Driver\Mysqli\Initializer\Options;
use PhpParser\Node\Expr\Cast\Array_;

#[ORM\Entity(repositoryClass: FacturaRepository::class)]
class Factura
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    private ?int $id = null;

    #[ORM\Column(length: 3)]
    private ?string $Establecimiento = null;

    #[ORM\Column(length: 3)]
    private ?string $PuntoEmision = null;
    
    #[ORM\Column]
    private ?int $Secuencial = null;

    #[ORM\Column(type: Types::DATETIME_MUTABLE)]
    private ?\DateTimeInterface $FechaEmision = null;

    #[ORM\Column(type: Types::DECIMAL, precision: 18, scale: 2)]
    private ?string $Subtotal = null;

    #[ORM\Column(type: Types::DECIMAL, precision: 18, scale: 2)]
    private ?string $Impuesto = null;

    #[ORM\Column(type: Types::DECIMAL, precision: 18, scale: 2)]
    private ?string $Total = null;

    #[ORM\OneToMany(mappedBy: 'IdFactura', targetEntity: Pago::class, orphanRemoval: true, fetch: 'EAGER')]
    private Collection $ListaPago;

    #[ORM\ManyToMany(targetEntity: Guia::class, mappedBy: 'ListaFactura', fetch: 'EAGER')]
    private Collection $ListaGuia;

    public function __construct()
    {
        $this->ListaPago = new ArrayCollection();
        $this->ListaGuia = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getEstablecimiento(): ?string
    {
        return $this->Establecimiento;
    }

    public function setEstablecimiento(string $Establecimiento): self
    {
        $this->Establecimiento = $Establecimiento;

        return $this;
    }

    public function getPuntoEmision(): ?string
    {
        return $this->PuntoEmision;
    }

    public function setPuntoEmision(string $PuntoEmision): self
    {
        $this->PuntoEmision = $PuntoEmision;

        return $this;
    }

    public function getSecuencial(): ?int
    {
        return $this->Secuencial;
    }

    public function setSecuencial(int $Secuencial): self
    {
        $this->Secuencial = $Secuencial;

        return $this;
    }

    public function getFechaEmision(): ?\DateTimeInterface
    {
        return $this->FechaEmision;
    }

    public function setFechaEmision(\DateTimeInterface $FechaEmision): self
    {
        $this->FechaEmision = $FechaEmision;

        return $this;
    }

    public function getSubtotal(): ?string
    {
        return $this->Subtotal;
    }

    public function setSubtotal(string $Subtotal): self
    {
        $this->Subtotal = $Subtotal;

        return $this;
    }

    public function getImpuesto(): ?string
    {
        return $this->Impuesto;
    }

    public function setImpuesto(string $Impuesto): self
    {
        $this->Impuesto = $Impuesto;

        return $this;
    }

    public function getTotal(): ?string
    {
        return $this->Total;
    }

    public function setTotal(string $Total): self
    {
        $this->Total = $Total;

        return $this;
    }

    /**
     * @return Collection<int, Pago>
     */
    public function getListaPago(): Collection
    {
        return $this->ListaPago;
    }

    public function addListaPago(Pago $listaPago): self
    {
        if (!$this->ListaPago->contains($listaPago)) {
            $this->ListaPago->add($listaPago);
            $listaPago->setIdFactura($this);
        }

        return $this;
    }

    public function removeListaPago(Pago $listaPago): self
    {
        if ($this->ListaPago->removeElement($listaPago)) {
            // set the owning side to null (unless already changed)
            if ($listaPago->getIdFactura() === $this) {
                $listaPago->setIdFactura(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection<int, Guia>
     */
    public function getListaGuia(): Collection
    {
        return $this->ListaGuia;
    }

    public function addGuia(Guia $Guia): self
    {
        if (!$this->ListaGuia->contains($Guia)) {
            $this->ListaGuia->add($Guia);
            $Guia->addFactura($this);
        }

        return $this;
    }

    public function removeGuia(Guia $Guia): self
    {
        if ($this->ListaGuia->removeElement($Guia)) {
            $Guia->removeFactura($this);
        }

        return $this;
    }
}
