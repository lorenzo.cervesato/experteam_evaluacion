<?php

namespace App\Entity;

use App\Repository\PagoRepository;
use Doctrine\DBAL\Types\Types;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity(repositoryClass: PagoRepository::class)]
class Pago
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    private ?int $id = null;

    #[ORM\Column(length: 10)]
    private ?string $TipoPago = null;

    #[ORM\Column(type: Types::DECIMAL, precision: 18, scale: 2)]
    private ?string $Valor = null;

    #[ORM\ManyToOne(inversedBy: 'ListaPago', fetch: 'EAGER')]
    #[ORM\JoinColumn(nullable: false)]
    private ?Factura $IdFactura = null;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getTipoPago(): ?string
    {
        return $this->TipoPago;
    }

    public function setTipoPago(string $TipoPago): self
    {
        $this->TipoPago = $TipoPago;

        return $this;
    }

    public function getValor(): ?string
    {
        return $this->Valor;
    }

    public function setValor(string $Valor): self
    {
        $this->Valor = $Valor;

        return $this;
    }

    public function getIdFactura(): ?Factura
    {
        return $this->IdFactura;
    }

    public function setIdFactura(?Factura $IdFactura): self
    {
        $this->IdFactura = $IdFactura;

        return $this;
    }
}
