<?php

namespace App\Entity;

use App\Repository\GuiaRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\DBAL\Types\Types;
use Doctrine\ORM\Mapping as ORM;
use App\Entity\Factura;
use Symfony\Component\Validator\Constraints as Assert;

#[ORM\Entity(repositoryClass: GuiaRepository::class)]
class Guia
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    private ?int $id = null;

    #[Assert\NotBlank(message: "El número de guía no puede estar vacío.")]
    #[Assert\Type(type: 'numeric', message: "El número de guía tiene que ser numerico")]
    #[ORM\Column(length: 10)]
    private ?string $NumeroGuia = null;

    #[Assert\NotBlank(message: "La fecha de envío no puede estar vacía.")]
    #[ORM\Column(type: Types::DATETIME_MUTABLE)]
    private ?\DateTimeInterface $FechaEnvio = null;

    #[Assert\NotBlank(message: "El país de origen no puede estar vacío.")]
    #[ORM\Column(length: 100)]
    private ?string $PaisOrigen = null;

    #[Assert\NotBlank(message: "El nombre del remitente no puede estar vacío.")]
    #[ORM\Column(length: 100)]
    private ?string $NombreRemitente = null;

    #[Assert\NotBlank(message: "La dirección del remitente no puede estar vacía.")]
    #[ORM\Column(length: 100)]
    private ?string $DireccionRemitente = null;

    #[Assert\Regex(pattern: "/^\+?[0-9]+$/", message: "El numero de telefono tiene que ser en un formato valido")]
    #[ORM\Column(length: 50, nullable: true)]
    private ?string $TelefonoRemitente = null;

    #[Assert\NotBlank(message: "El correo electrónico del remitente no puede estar vacío.")]
    #[ORM\Column(length: 50, nullable: true)]
    private ?string $EmailRemitente = null;

    #[Assert\NotBlank(message: "El país de destino no puede estar vacío.")]
    #[ORM\Column(length: 100)]
    private ?string $PaisDestino = null;

    #[Assert\NotBlank(message: "El nombre del destinatario no puede estar vacío.")]
    #[ORM\Column(length: 100)]
    private ?string $NombreDestinatario = null;

    #[Assert\NotBlank(message: "La dirección del destinatario no puede estar vacía.")]
    #[ORM\Column(length: 100)]
    private ?string $DireccionDestinatario = null;

    #[Assert\Regex(pattern: "/^\+?[0-9]+$/", message: "El numero de telefono tiene que ser en un formato valido")]
    #[ORM\Column(length: 50, nullable: true)]
    private ?string $TelefonoDestinatario = null;

    #[Assert\Email(message: "El correo electrónico debe ser válido.")]
    #[ORM\Column(length: 50, nullable: true)]
    private ?string $EmailDestinatario = null;

    #[Assert\NotBlank(message: "El monto total no puede estar vacío.")]
    #[Assert\Type(type: 'numeric')]
    #[ORM\Column(type: Types::DECIMAL, precision: 18, scale: 2)]
    private ?string $Total = null;

    #[ORM\ManyToMany(targetEntity: Factura::class, inversedBy: 'ListaGuia', fetch: 'EAGER')]
    private Collection $ListaFactura;

    public function __construct()
    {
        $this->ListaFactura = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getNumeroGuia(): ?string
    {
        return $this->NumeroGuia;
    }

    public function setNumeroGuia(string $NumeroGuia): self
    {
        $this->NumeroGuia = $NumeroGuia;

        return $this;
    }

    public function getFechaEnvio(): ?\DateTimeInterface
    {
        return $this->FechaEnvio;
    }

    public function setFechaEnvio(\DateTimeInterface $FechaEnvio): self
    {
        $this->FechaEnvio = $FechaEnvio;

        return $this;
    }

    public function getPaisOrigen(): ?string
    {
        return $this->PaisOrigen;
    }

    public function setPaisOrigen(string $PaisOrigen): self
    {
        $this->PaisOrigen = $PaisOrigen;

        return $this;
    }

    public function getNombreRemitente(): ?string
    {
        return $this->NombreRemitente;
    }

    public function setNombreRemitente(string $NombreRemitente): self
    {
        $this->NombreRemitente = $NombreRemitente;

        return $this;
    }

    public function getDireccionRemitente(): ?string
    {
        return $this->DireccionRemitente;
    }

    public function setDireccionRemitente(string $DireccionRemitente): self
    {
        $this->DireccionRemitente = $DireccionRemitente;

        return $this;
    }

    public function getTelefonoRemitente(): ?string
    {
        return $this->TelefonoRemitente;
    }

    public function setTelefonoRemitente(?string $TelefonoRemitente): self
    {
        $this->TelefonoRemitente = $TelefonoRemitente;

        return $this;
    }

    public function getEmailRemitente(): ?string
    {
        return $this->EmailRemitente;
    }

    public function setEmailRemitente(string $EmailRemitente): self
    {
        $this->EmailRemitente = $EmailRemitente;

        return $this;
    }

    public function getPaisDestino(): ?string
    {
        return $this->PaisDestino;
    }

    public function setPaisDestino(string $PaisDestino): self
    {
        $this->PaisDestino = $PaisDestino;

        return $this;
    }

    public function getNombreDestinatario(): ?string
    {
        return $this->NombreDestinatario;
    }

    public function setNombreDestinatario(string $NombreDestinatario): self
    {
        $this->NombreDestinatario = $NombreDestinatario;

        return $this;
    }

    public function getDireccionDestinatario(): ?string
    {
        return $this->DireccionDestinatario;
    }

    public function setDireccionDestinatario(string $DireccionDestinatario): self
    {
        $this->DireccionDestinatario = $DireccionDestinatario;

        return $this;
    }

    public function getTelefonoDestinatario(): ?string
    {
        return $this->TelefonoDestinatario;
    }

    public function setTelefonoDestinatario(?string $TelefonoDestinatario): self
    {
        $this->TelefonoDestinatario = $TelefonoDestinatario;

        return $this;
    }

    public function getEmailDestinatario(): ?string
    {
        return $this->EmailDestinatario;
    }

    public function setEmailDestinatario(?string $EmailDestinatario): self
    {
        $this->EmailDestinatario = $EmailDestinatario;

        return $this;
    }

    public function getTotal(): ?string
    {
        return $this->Total;
    }

    public function setTotal(string $Total): self
    {
        $this->Total = $Total;

        return $this;
    }

    /**
     * @return Collection<int, Factura>
     */
    public function getListaFactura(): Collection
    {
        return $this->ListaFactura;
    }

    public function addFactura(Factura $factura): self
    {
        if (!$this->ListaFactura->contains($factura)) {
            $this->ListaFactura->add($factura);
        }

        return $this;
    }

    public function removeFactura(Factura $factura): self
    {
        $this->ListaFactura->removeElement($factura);

        return $this;
    }
}
