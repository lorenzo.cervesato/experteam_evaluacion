<?php

namespace App\Repository;

use App\Entity\Guia;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @extends ServiceEntityRepository<Guia>
 *
 * @method Guia|null find($id, $lockMode = null, $lockVersion = null)
 * @method Guia|null findOneBy(array $criteria, array $orderBy = null)
 * @method Guia[]    findAll()
 * @method Guia[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class GuiaRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Guia::class);
    }

    public function save(Guia $entity, bool $flush = false): void
    {
        $this->getEntityManager()->persist($entity);

        if ($flush) {
            $this->getEntityManager()->flush();
        }
    }

    public function remove(Guia $entity, bool $flush = false): void
    {
        $this->getEntityManager()->remove($entity);

        if ($flush) {
            $this->getEntityManager()->flush();
        }
    }

    public function findDistinctPaisOrigen()
    {
        return $this->createQueryBuilder('g')
            ->select('DISTINCT g.PaisOrigen')
            ->getQuery()
            ->getResult();
    }
    public function findDistinctPaisDestino()
    {
        return $this->createQueryBuilder('g')
            ->select('DISTINCT g.PaisDestino')
            ->getQuery()
            ->getResult();
    }

//    /**
//     * @return Guia[] Returns an array of Guia objects
//     */
//    public function findByExampleField($value): array
//    {
//        return $this->createQueryBuilder('g')
//            ->andWhere('g.exampleField = :val')
//            ->setParameter('val', $value)
//            ->orderBy('g.id', 'ASC')
//            ->setMaxResults(10)
//            ->getQuery()
//            ->getResult()
//        ;
//    }

//    public function findOneBySomeField($value): ?Guia
//    {
//        return $this->createQueryBuilder('g')
//            ->andWhere('g.exampleField = :val')
//            ->setParameter('val', $value)
//            ->getQuery()
//            ->getOneOrNullResult()
//        ;
//    }
}
