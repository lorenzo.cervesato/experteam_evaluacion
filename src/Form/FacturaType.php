<?php

namespace App\Form;

use App\Entity\Factura;
use App\Entity\Guia;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class FacturaType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('Establecimiento', TextType::class, [])
            ->add('PuntoEmision', TextType::class, [])
            ->add('Secuencial', NumberType::class, [])
            ->add('FechaEmision', DateType::class, [
                'widget' => 'single_text'
            ])
            ->add('ListaGuia', ChoiceType::class, [
                'choices'  => $options['map_guias'],
                'multiple' => true,
                'expanded' => false,
                'mapped' => false
            ])
            ->add('Guardar', SubmitType::class, [])
        ;
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setRequired('map_guias');
        
        $resolver->setDefaults([
            'data_class' => Factura::class,
        ]);
    }
}
