<?php

namespace App\Form;

use App\Entity\Guia;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\TelType;
use Symfony\Component\Form\Extension\Core\Type\MoneyType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;

class GuiaType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('NumeroGuia', TextType::class, [])
            ->add('FechaEnvio', DateType::class, [
                'widget' => 'single_text'
            ])
            ->add('PaisOrigen', TextType::class, [])
            ->add('NombreRemitente', TextType::class, [])
            ->add('DireccionRemitente', TextType::class, [])
            ->add('TelefonoRemitente', TelType::class, [])
            ->add('EmailRemitente', EmailType::class, [])
            ->add('PaisDestino', TextType::class, [])
            ->add('NombreDestinatario', TextType::class, [])
            ->add('DireccionDestinatario', TextType::class, [])
            ->add('TelefonoDestinatario', TelType::class, [])
            ->add('EmailDestinatario', EmailType::class, [])
            ->add('Total', MoneyType::class, [
                'currency'=>'USD'
                ])
            ->add('Guardar', SubmitType::class, [])
        ;
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => Guia::class,
        ]);
    }
}
