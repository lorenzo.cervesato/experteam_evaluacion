<?php

namespace App\Form;

use App\Entity\Pago;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\MoneyType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;

class PagoType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('TipoPago', ChoiceType::class, [
                'choices'  => ['Efectivo' => 'Efectivo', 'Cheque' => 'Cheque', 'Tarjeta' => 'Tarjeta'],
                'expanded' => false,
                'mapped' => false
            ])
            ->add('Valor', MoneyType::class, [
                'currency'=>'USD'
                ])
            ->add('IdFactura', ChoiceType::class, [
                'choices'  => $options['map_facturas'],
                'multiple' => false,
                'expanded' => false,
                'mapped' => false
            ])
            ->add('Guardar', SubmitType::class, [])
        ;
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setRequired('map_facturas');

        $resolver->setDefaults([
            'data_class' => Pago::class,
        ]);
    }
}
